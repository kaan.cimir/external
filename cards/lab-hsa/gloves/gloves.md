---
mount: lab/gloves
name: "Gloves"
tags:
  - lab/ppe
redirects:
  - lab:gloves
---

# Gloves

When working in the laboratories, you may have to wear gloves to protect yourself from the different hasards present in the laboratories or to protect your samples from contaminations.

Not sure which glove to use?  
**Consult the chemical's Safety Data Sheet (SDS) or the [safety officer](https://service.uni.lu/sp?id=sc_cat_item&sys_id=f140d745dbe83c905c72ef3c0b96194e&sysparm_category=cca7f2c1db683c905c72ef3c0b961940) for guidance.**


## One Glove Policy

At LCSB, we follow the one glove policy regarding door handles and elevator buttons. 
One glove for carrying your samples and one hand without a glove to open doors and push elevator buttons.

  ![img5](img/img5.jpg){.align-center}


**Do not touch the elevator button or the door handle with your gloves!**


  ![img1](img/img1.jpg){.align-center}


Exception is done for the doors marked by this sign:

  ![Gloves3.png](img/Gloves3.png){.align-center}


For microscopes, computers and keyboard: stickers on the instruments indicate if gloves are required.

  ![Gloves1.png](img/Gloves1.png){.align-center}


## General Rules

### Before starting to work

Integrity of the gloves should be checked each time you put on new gloves. 
The size of the gloves has to fit to ensure good protection.

### While working

In BSL-1 and BSL-2 Labs: Only work with gloves in the laboratory.
Change both gloves between different lab levels (BSL-1 or BSL-2)

Important: Remind yourself why you are wearing gloves!  
Gloves protect you from chemicals and biological material, and they protect your samples from contaminations.

  - **Do not touch your face, hair, skin with your gloves.**

  ![img4](img/img4.jpg){.w2-5 .align-center}


  - **Do not touch your badge, phone with your gloves.**


::: centered-block
  ![img3](img/img3.jpg){.w2-5}
  ![img2](img/img2.jpg){.w2-5}
:::

### When finished working

Disposable gloves must be discarded after each use or when they become contaminated.  
If the gloves are not contaminated, discard them in the domestic waste bins of the lab. 
Otherwise, follow the [chemical and biological waste concept](/lab/waste).

It is not that trivial to safely wear and remove disposable gloves without becoming contaminated, please watch the video on the [DOCEBO platform](https://unisupport.docebosaas.com/learn/course/52/play/332:103/how-to-wear-and-remove-gloves;lp=3).