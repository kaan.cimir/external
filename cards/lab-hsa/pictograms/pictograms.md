---
mount: lab/pictograms
name: "Pictograms"
tags:
  - lab/hsa
  - lab/chemicals
redirects:
  - lab:pictograms
  - /external/lab-hsa/pictograms/
  - /cards/lab-hsa:pictograms
  - /external/cards/lab-hsa:pictograms
  - /external/external/lab-hsa/pictograms/
  - /cards/lab:pictograms
  - /external/cards/lab:pictograms
---


# Pictograms

Chemical risks in research laboratories are important to consider due to the numerous chemicals used and their various mixture. Knowing the signification of the hazard pictograms is therefore crucial.

Here is a summary of the pictograms that can be encountered in the laboratories. For more detailed information, please refer to the [Table of concordance](https://www.yumpu.com/fr/document/read/19512777/produits-d-angereux-nouvel-etiquetage-tableau-de-concordance-1) (currently only in French).

To properly label your solutions, you can find some labels with the pictograms in the common stock of BT1 (-01 floor) and of BT2 (-02 floor).

## Health Hazard - CMR (Carcinogenic, Mutagenic, Reprotoxic) substances

- Carcinogens
- Mutagenicity
- Reproductive Toxicity
- Respiratory Sensitizer
- Target Organ Toxicity
- Aspiration Toxicity

![img8.png](img/img8.png){.w1-3 .align-center}

## Flame

- Flammables
- Pyrophoric
- Self-heating
- Emits flammable Gas
- Self-reactive
- Organic Peroxides

![img3.png](img/img3.png){.w1-3 .align-center}

## Exclamation Mark

- Irritant (eye, skin)
- Skin Sensitizer
- Acute Toxicity
- Narcotic Effects
- Respiratory tract irritant
- Hazardous to the Ozone layer

![img7.png](img/img7.png){.w1-3 .align-center}

## Gas Cyclinder

- Gas under pressure

![img2.png](img/img2.png){.w1-3 .align-center}

## Corrosive

- Skin corrosion/burns
- Eye damage
- Corrosive to metal

![img5.png](img/img5.png){.w1-3 .align-center}

## Exploding Bomb

- Explosives
- Self-reactive
- Organic Peroxides

![img1.png](img/img1.png){.w1-3 .align-center}

## Flame over circle

- Oxidizers

![img4.png](img/img4.png){.w1-3 .align-center}

## Skull and crossbones

- Acute toxicity (Fatal or toxic)

![img6.png](img/img6.png){.w1-3 .align-center}

## Environment

- Aquatic toxicity

![img9.png](img/img9.png){.w1-3 .align-center}
