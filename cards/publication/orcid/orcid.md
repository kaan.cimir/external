---
mount: publication/orcid
name: "Obtain an ORCID"
tags:
  - publication/resources
redirects:
  - publication:orcid
  - /external/publication/orcid/
  - /cards/publication:orcid
  - /external/cards/publication:orcid
  - /external/external/publication/orcid/
---


# Obtain an ORCID

An ORCID (Open Researcher and Contributor ID) is a 12 digits code used to identify researchers.

To obtain an ORCID, click [here](https://orcid.org/register).

## Register your ORCID on the publications platform

1. Go to the [LCSB publications platform](http://publications.lcsb.uni.lu)

2. Click on **My profile** on the top right of the page

    ![img1.png](img/img1.png)

3. Select the edit button (button with a pencil)

    ![img2.png](img/img2.png)

4. Add the ORCID number as 0000-1234-5678-9123

5. Click on **Save**
