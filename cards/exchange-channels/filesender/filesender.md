---
mount: exchange-channels/filesender
name: "FileSender"
tags:
  - exchange-channels
  - data
---

# FileSender 
## Overview 
FileSender is a web-based application hosted by [Restena](https://restena.lu/), which supports the transfer of files securely and easy by uploading the files directly to the Restena server, and  sending an e-mail invitation containing a personalised link to the user(s). 

LCSB-affiliated researchers can use FileSender by logging in with their UNI credentials. However, if you wish to receive data from external collaborators via FileSender, you must share an invitation link to the collaborators in advance. In this case, please go to the section "[Invite external collaborators to transfer data via FileSender](#invite-external-collaborators-to-transfer-data-via-filesender)" for further information. 

Important Features: 
- End-to-end encryption, facilitating secure data transfer between institutes, partners, etc. 
- Maximum 100 GB per transfer. 
- External collaboration via invitations without need to login.
- Transfer and invitation history.  
- Automatic deletion of data after expiration date.  

## Get started
1. Go to [FileSender](https://fs.restena.lu/) hosted by Restena.
2. Select "University of Luxembourg".
![unilu-fs-instance.png](img/unilu-fs-instance.png)
3. Login with UNI credentials when directed to the EduID authentication page. 
![login-uni-credentials.png](img/login-uni-credentials.png)
4. You can now start to use FileSender to transfer data. 
![filesender-home-page.png](img/filesender-home-page.png)

## Transfer data with FileSender 
In the following steps we provide instructions on how to transfer encrypted data with FileSender. 

1. Use the *"drag & drop"* feature or the button *"Select files"* to upload the data. 
![fs-upload.png](img/fs-upload.png){.align-center}
2. Enter the recipient's email. For multiple recipients please separate by a comma or semi-colon.
Please double-check that you have entered the correct email addresse(s), because whoever receives this email will have access to the data. 
![fs-recipient-email.png](img/fs-recipient-email.png){.w1-2 .align-center}
3. OPTIONAL: Write a subject and message to the recipient.
4. [**IMPORTANT**]{.highlight}: Tick the box "File Encryption". This is a requirement, when transferring confidential and/or sensitive human data. 
5. Enter a strong encryption password. You can use the password generator provided by FileSender.
![file-encryption.png](img/file-encryption.png){.w1-2 .align-center}
6. Copy password and share it securely via PrivateBin. Please follow the [How-to Card on sending password securely](/exchange-channels/sharingPasswords). FileSender does not store passwords, when you leave this page, you will not be able to see the password.
![fs-notifications.png](img/fs-notifications.png){.w1-2 .align-center}
7. OPTIONAL: To receive notifications for the data transfer, tick the relevant boxes as shown below.  
8. Press the send button. The recipient(s) will receive an email from FileSender containing a personalised link to the data. 

## Download data from FileSender
In the following steps we provide instructions on how to download data from FileSender. 
1. Open the download link in the invitation email sent from FileSender.  
![fs-email.png](img/fs-email.png){.align-center}
2. You will be directed to the web application of FileSender. Login with your UNI.LU credentials and press "Download" as shown below. <br>
![fs-download.png](img/fs-download.png){.align-center}
3. Enter the encryption password shared via PrivateBin.
![enter-password.png](img/enter-password.png){.align-center}
4. FileSender will automatically download the files locally on your computer and display "Download complete" when finished. 
5. Per default, the data provider will be notified  about successful download via email.

## Invite external collaborators to transfer data via FileSender
To use FileSender you must authenticate yourself by logging in with your UNI credentials. However, if you want to receive encrypted data via FileSender from external collaborators outside of LCSB/University of Luxembourg, you can sent a guest voucher, which allows them to transfer data via FileSender. In the following steps, we provide instructions on how to send a guest voucher to external collaborators, enabling them to transfer data to LCSB premises. 

1. Once logged into FileSender, go to the "Guest"-tab. 
![fs-voucher.png](img/fs-voucher.png){.align-center}
2. Enter the e-mail address of the external collaborator, whom you wish to receive data from via FileSender. 
3. OPTIONAL: Write a subject and message to the external collaborator.
4. Push "Send Voucher". 
5. The external collaborator will receive an e-mail to upload data to FileSender as shown below. 
![fs-email-voucher](img/fs-email-voucher.png){.align-center}
