---
mount: integrity/dmp
name: "Data Management Plan"
tags:
  - integrity
redirects:
  - integrity:dmp
  - /external/integrity/dmp/
  - /cards/integrity:dmp
  - /external/cards/integrity:dmp
  - /external/external/integrity/dmp/
---


# Data Management Plan

![lcsb_rd-lifecycle.png](img/lcsb_rd-lifecycle.png){.w3-4}

Data management planing is a process of defining and documenting strategy for management of research data in each step of research data lifecycle throughout the research project. The main goal of the process is to create concise and complete document (Data Management Plan - DMP) which helps all involved stakeholders to avoid unexpected and unnecessary problems before, during and after the project. Data management plan also contributes to adoption of best data management practices and as such it is increasingly required by funders along the project proposal.

## Best practices when writing DMP

* **Do not copy paste** - Blindly accepting existing content can be error prone and does not help the research team to review the project specific settings.
* **Update it regularly** - As the research project progresses, new aspects (infrastructure, partners, data uses, ...) should be reflected in the DMP to allow effective adoption of changes.
* **Start in time** - Data management requirements have implications on all downstream processes including negotiations of legal documents and setup of necessary infrastructure. Therefore, the DMP should be revised as soon as possible after the funding is granted.
* **Get help** - Collaboration with institutional support services is crucial for correctness and completeness of the planning.
* **Share it** - Communicating data management plan in timely manner to all members of the project, partners and other involved stakeholders contributes to transparency of the planned operations.

## Data management planning at LCSB

There are several options for data management planning:

|Name | Description | Pros | Cons | Choose when: |
| ------ | ----------- | ------ | ------ | ----- |
| [DSW Elixir Luxembourg](https://elixir-lu.ds-wizard.org/) | Instance of [Data Stewardship Wizard](https://ds-wizard.org/) tool facilitating detailed data management planning with overall awareness raising. | Includes guidance on questions, external references Complex questionnaires with branching logic  Collaborative editing.  Comments and TODOs  Multiple sharing options  Supports university credentials (via LS Login)  Horizon Europe and Science Europe export templates (FNR template in preparations)  Versioning and several multiple format export| Require user to familiarize themselves with the tool environment  Questionnaire can seem exhaustive | Choose when you want to responsibly perform and manage very detailed and structured data management plan while raising awareness of all aspects of research data management life cycle. |
| [DMPOnline](https://unilu.dmponline-mt.dcc.ac.uk) provided by University of Luxembourg  [DMPRoadmap](https://dmponline.elixir-luxembourg.org/) provided by Elixir Luxembourg | Instances of identical tools for simple and effective tool for data management plan document compilation. | Includes guidance on questions.  Supports versioning and multiple format export.  Comments and sharing.  Supports FNR export template. ​| Questions are rather general as they follow template provided by funder. | Choose when you are experienced in data management planning and you need a simpler tool allowing fast compilation of the document. |
| Document provided by funder | Funders requiring data management plan usually provide document template. See example of templates for [FNR](https://storage.fnr.lu/index.php/s/urQOCMeKlgXexZF) or [Horizon Europe](https://enspire.science/wp-content/uploads/2021/09/Horizon-Europe-Data-Management-Plan-Template.pdf). | Format of the document is matching the funder requirements | Questions are rather general.   Limited guidance. | Choose if you are performing the planning alone and your project is not complex from data management point of view. |

 ([LCSB data stewards](mailto:lcsb-datastewards@uni.lu)) provide full support on data management planing including the tools, trainings and DMP reviews.

## References

- Pergl, R., Hooft, R., Suchánek, M., Knaisl, V., & Slifka, J. (2019). "Data Stewardship Wizard": A Tool Bringing Together Researchers, Data Stewards, and Data Experts around Data Management Planning. Data Science Journal, 18. <https://doi.org/10.5334/dsj-2019-059>
- ELIXIR (2021) Research Data Management Kit. A deliverable from the EU-funded ELIXIR-CONVERGE project (grant agreement 871075). URL: <https://rdmkit.elixir-europe.org>
