---
mount: lab/balances
name: "Balances: utilization and maintenance"
tags:
  - lab/equipment
redirects:
  - lab:balances
  - /external/lab-equipment/balances/
  - /cards/lab-equipment:utilization-of-balances
  - /external/cards/lab-equipment:utilization-of-balances
  - /lab/utilization-of-balances
  - /external/external/lab-equipment/utilization-of-balances/
  - /cards/lab:utilization-of-balances
  - /external/cards/lab:utilization-of-balances
  - /cards/lab-equipment:balances
  - /external/cards/lab-equipment:balances
  - /external/external/lab-equipment/balances/
  - /cards/lab:balances
  - /external/cards/lab:balances
---


# Balances: utilization and maintenance

Two balances types are available at LCSB:
- XPE205DR (1.4 mg to 220 g)
- XS4002S (820 mg to 4.1 kg)

To ensure reproducible science, precautions should be taken for the preparation of reagents and solutions.
Balances are at the beginning of a lot of processes, in order to keep their precision, the balances must always be kept clean and tidy.
Not only to please your colleagues, but mainly to guarantee the quality of your work.

## Personal protective equipment

Always wear a lab coat, gloves and safety glasses when using the balances.

[If weighing dangerous, volatile compounds, use a balance located under a chemical hood.]{.color-red}

There are such balances in fume hoods on 
- the 5th floor in BT1,
- the 1st floor in BT2 (in a [Safety Weighing Cabinet](/lab/weighing-cabinet)).

## Weighing

**XPE205DR: used for 1.4 mg to 220 g**

![XPE205DR.jpg](img/XPE205DR.jpg){.w2-3 .align-center}

**XS4002S: used for 820 mg to 4.1 kg**

![XS4002S.jpg](img/XS4002S.jpg){.w2-3 .align-center}

1. For XPE balances, check that the air bubble is in the middle of the circle.
   It is located on the rear right foot of the instrument.

   ![XPE205DR_2.jpg](img/XPE205DR_2.jpg){.w3-4 .align-center}

2. Switch on the balance by pressing on “On/Off” until the display appears.
3. In order to allow an equilibration of electronics within the balance, allow it to stand for 10 to 15 minutes before starting to use it.
4. Zero the balance by pressing on “0”.
    a. For the XPE balance, make sure the doors are closed by pressing on “↨” or by passing your hand over the right and left upper parts of the terminal.
5. Place an anti-static weighing boat or a weighing paper in the middle of the pan of the balance. Do not place the material to be weighted directly on the pan.
6. If you weigh directly in a tube, place the Ergo Clip on the pan of the balance and place your tube in it.

![Ergo-Clip_2.jpg](img/Ergo-Clip_2.jpg){.w3-4 .align-center}

7.	Tare the balance by pressing on “T”
8.	Place the material to be weighted in the anti-static weighing boat or on the weighing paper in the middle of the pan to avoid corner-load error. For the XPE balance,close the doors after loading your material.
9.	To prevent contamination of stock material, do not return unused substance to the stock bottle.
10.	After use, switch the balance off by pressing on the “ON/OFF” button.
11.	Contact the Quality Officer if the logbook is full.
12.	If you notice anything wrong with the instrument, open a [ticket](https://service.uni.lu/sp?id=sc_cat_item&sys_id=be0873f8db7070505c72ef3c0b96199f&sysparm_category=af924f17dbac38105c72ef3c0b96194b).

## Cleaning

After each use, the balance and the material used to weight have to be cleaned.

### XPE205DR
- Open the doors and remove the weighing pan
- Wipe the surfaces with a brush
- Wipe the weighing pan and the inside of the chamber with a damp cloth
- Place the weighing pan back and make sure it is correctly placed

### XS4002S
- Wipe the surfaces with a brush
- Wipe the weighing pan with a damp cloth
- If some powder fell off the pan, carefully remove the crown around the pan and clean it to remove any residue. If you need assistance, contact the instrument responsible person.
    ![Crown.jpg](img/Crown.jpg)
- When the crown is removed, carefully wipe with a damp cloth around the weighing pan
- Carefully place the crown back
- Always wipe the area around the balance with 70 % ethanol to remove any substance
- Spoons: wash them with a mild detergent and water
