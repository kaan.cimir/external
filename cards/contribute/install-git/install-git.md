---
mount: contribute/install-git
name: "Installation of Git"
tags:
  - contribute/git
redirects:
  - contribute:install-git
  - /external/contribute/install-git/
  - /cards/contribute:install-git
  - /external/cards/contribute:install-git
  - /external/external/contribute/install-git/
---


# Installation of Git

This card describes how to install the commandline tool `git`.

Alternatively, several other git clients are [listed in a separate card](/contribute/git-clients), but for many of these, the `git` command is required to be already installed on the system.

## Downloading git

You can find installers for Windows, MacOS and Linux on the [Git webpage](https://git-scm.com/downloads).

## Verifying the functionality

Start you terminal, and type in:

```sh
git version
```

That should tell you your installed git's version, such as `git version 2.43.0`.

**Note:** On GNU/Linux, BSDs, MacOS and WSL (Windows subsystem for Linux), basically any terminal will work. On Windows, it is recommended that you use *Git Bash* shell, which comes with the git installation package, and is usually properly configured. Other shells (such as `cmd.exe` and PowerShell) may not work at all.

## Validating the host keys

If you are using Visual Studio Code (or other clients) and plan to contribute to a repository hosted on the [LCSB GitLab](https://gitlab.lcsb.uni.lu), you will need to manually verify the host keys before you are able to clone or push to a repository.

To do that, open a terminal (or Git Bash on Windows), and run this command:
```bash
ssh git@gitlab.lcsb.uni.lu -p8022
```

The command prints a hostkey of the LCSB GitLab, which should match the one you are expecting (in February 2024, the hostkey was: `SHA256:g4pablIHyKZQWOXkBFf7XOBZuOHDlRKJF3c84sXfSkA`).

To accept the hostkey, simply type `yes` and press enter. This will add the GitLab server to the list of known hosts in your system, allowing all git clients to connect securely.
