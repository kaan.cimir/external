---
mount: contribute/supersede
name: "Taking over a stale merge request"
tags:
  - contribute/workflows
redirects:
  - contribute:supersede
  - /external/contribute/supersede/
  - /cards/contribute:supersede
  - /external/cards/contribute:supersede
  - /external/external/contribute/supersede/
---


# Taking over a stale merge request

In some cases, it is required to continue to work on a merge request which was
opened by someone else. Typically, this is problematic especially if the merge
request source branch is in a separate fork, because other people generally can
not push new commits into forks that belong to others.

The overall workflow is as follows:

1. Use the GitLab "merge request checkout" to retrieve the code
2. Copy the code to your own new branch
3. Close the original merge request and replace it with a new one.

## Prerequisities

You will need the following:

- A terminal with `git` [installed](/contribute/install-git) (e.g., the one available in VSCode, as in the picture below)

    ![getting a terminal in VSCode](img/vscode-terminal.png)

- Merge request number of the original merge request
- A place to push new code -- either a developer access to the target repository, or your own fork thereof

## Step 1: Get the original code

To download the original merge request ID, first ensure the following:

- find the merge request ID of the request you want to take over (here we will use `1234` for demonstration)
- decide on the branch name where you want the code checked out (we will use `xx-some-feature` for demonstration -- remember to name the branch precisely and use your initials at the beginning instead of the `xx`)
- make sure you have the merge request target repository in git remotes -- you can check that using `git remote -v`

If you do not have the repository there, you can add it as follows:

1. Go to the repository main page, click *Clone* and copy the SSH clone URL, as seen on pictures below:

    ![finding the "clone" button](img/clone-button.png)

    ![getting the SSH clone URL from GitLab](img/ssh-url.png)

2. Run command `git remote add upstream SSH_CLONE_URL_HERE` in your terminal
   (obviously, replace the `SSH_CLONE_URL_HERE` with the actual clone URL --
   the complete command may look e.g. like `git remote add upstream
   ssh://git@gitlab.lcsb.uni.lu:8022/lcsb/howto/external.git`)

From now, we will assume the repository is added as `upstream`. If you do not have a
fork, it may also be called `origin` or similarly -- in any case, simply check
using `git remote -v`.

Having everything set up, you can download the code to a new branch using the
following template:

```sh
git fetch REMOTE merge-requests/MR_ID/head:YOUR_BRANCH
git checkout YOUR_BRANCH
```

Upon replacing the placeholders with example values, the complete command may
look as follows:

```sh
git fetch upstream merge-requests/1234/head:xx-some-features
git checkout xx-some-features
```

If everything worked right, you should now be on the same branch as the author of the original merge request.

## Step 2: Pushing your branch and opening a new merge request

To upload your code to GitLab, simply follow with a push:

```
git push
```

(The command may instruct you to additionally specify the correct remote to
upload the code to, but you will be instructed via the message. Final command
may look like `git push -u origin xx-some-features` or similar.)

## Step 3: Replace a new merge request

In GitLab, open a new merge request from the newly created branch (you will
likely be able to use the green banner that appears automatically upon pushing
new branches).

In description, clearly specify that this merge request supersedes the original
one, and include a link to the original. E.g. write something like:

> This MR supersedes !1234.

(The merge requests automatically are linked by GitLab if you write the number
prefixed by the exclamation mark `!`.)

In the original MR, appropriately write a new comment that the MR is superseded
with the new one:

> This MR is superseded by !54321.

After that, you can close the original merge request.
