---
mount: contribute/vscode
name: "Contribute using Visual Studio Code"
tags:
  - contribute/git
redirects:
  - contribute:vscode
  - /external/contribute/vscode/
  - /cards/contribute:vscode
  - /external/cards/contribute:vscode
  - /external/external/contribute/vscode/
---


# Contribute using Visual Studio Code

This guide shows how to use Visual Studio Code to write a How-to Card:

## Step 0: Update your fork

Please update your fork first, by View > Command Palette and searching for 'Git: Fetch from all remotes'.

## Step 1: Create a new branch

For each How-to Card, it is strongly recommended to create a new branch.

- Click on:
    ![visual-code_img_1.png](img/visual-code_img_1.png)  (on the bottom left)

- Click on: `create new branch from...`

    ![visual-code_img_2.png](img/visual-code_img_2.png)

- Add a name of the branch (e.g., name of the how-to card) and press enter.

- Choose `upstream/develop`.

    ![visual-code_img_3.png](img/visual-code_img_3.png)

- The new branch is created as can be seen on the bottom left of the screen.

    ![visual-code_img_4.png](img/visual-code_img_4.png)

## Step 2: Create a new folder

- To create a new folder, browse to `howto-cards-internal`

    ![visual-code_img_5.png](img/visual-code_img_5.png)

- Then, select the `internal` folder:

    ![visual-code_img_6.png](img/visual-code_img_6.png)

- Click on new folder (the name of the folder should be the name of the howto card). Please only use lowercase letters.

    ![visual-code_img_7.png](img/visual-code_img_7.png)

## Step 3: Create a new file

- Click on `New file`. Name the file the same as the folder using lowercase letters linked by dashes

    ![visual-code_img_8.png](img/visual-code_img_8.png)

## Step 4: Write the procedure in Markdown

If you need more details on how to do this, refer to the [corresponding howto-card](/contribute/markdown).

## Step 5: Commit your changes

- on the left side click on:  ![visual-code_img_11.png](img/visual-code_img_11.png)

- below changes click on:

    ![visual-code_img_12.png](img/visual-code_img_12.png)

    stated changes

- add a message (description of your changes) and click "commit"

    ![visual-code_img_13.png](img/visual-code_img_13.png)

## Step 6: Push to the LCSB Gitlab server

- When you push your procedure for the first time click on the cloud icon:

    ![visual-code_img_15.png](img/visual-code_img_15.png)

- Choose origin/"your gitlab user name".
- Open [Gitlab:](https://gitlab.lcsb.uni.lu/)

    ![visual-code_img_16.png](img/visual-code_img_16.png)

- To synchronize changes:

    ![visual-code_img_17.png](img/visual-code_img_17.png)
