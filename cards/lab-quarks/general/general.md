---
mount: lab/general
name: "General information on Quarks"
tags:
  - lab/software/quarks
redirects:
  - lab:general
  - /external/lab-quarks/general/
  - /cards/lab-quarks:quarks-general
  - /external/cards/lab-quarks:quarks-general
  - /external/external/lab-quarks/quarks-general/
  - /cards/lab:quarks-general
  - /external/cards/lab:quarks-general
  - /cards/lab-quarks:general
  - /external/cards/lab-quarks:general
  - /external/external/lab-quarks/general/
  - /cards/lab:pH-general
  - /external/cards/lab:general
---


# General information on Quarks

## How to access Quarks?

[https://quarks.lcsb.uni.lu](https://quarks.lcsb.uni.lu)

### What does SSO connection mean?

SSO connection means connection with Uni credentials: name.lastname@uni.lu and associated password.
VPN connection is required if not connected to the local network.

Use the Quarks website with the computer to enter the new batch number and final location of the product.
Use the Quarks application to enter the batch number and final location of the chemical products received (by scanning).

## How is the Quarks flow organized at LCSB?

1. Ordering an article via the purchase platform in Quarks.
2. If it is a chemical, the product is automatically created in the chemical product inventory of Quarks. Biological reagents and medication are also automatically created in the chemical product inventory.
3. Reception of the article by support team. If it is a chemical, the label is printed and placed in the dedicated box.
4. The buyer comes to pick up the deliveries and the labels.
5. The buyer

     - Use the Quarks website with the computer to enter the new batch number and final location of the product.
     - Use the Quarks application to enter the batch number and final location of the chemical products received (by scanning).

## How are the Quarks devices organized?

- Computers of the lab or personal computers allow to access the website.
- Scanners also called the TC21 allow to use the Quarks application in order to move, discard or get information about a product by scanning its Quarks label.
For more information on the TC21, please refer to corresponding [How-to card](https://howto.lcsb.uni.lu/lab/quarks-stock-management/#using-tc21-scanner).

![40.png](img/40.png)

- In the laboratories, you can also acces the website or the application using the tablets.
- Scanners and tablets have charging support to ensure it is always charged.

![41.png](img/41.png)
![42.png](img/42.png)

- The printers available in the laboratories are linked via USB to the adjacent computer.
Those printers are also linked to the network of the University.

## What are the roles in Quarks?

Quarks gives us the possibility to have different roles profiles, with different rights.

**Viewer**: only view orders from own group.

**Researcher**: place article in a cart and view orders from/for own group, create new article.

**Technician**: place article in a cart and view orders from/for own group, create new articles, receiving notifications on status of all orders.

**Budget responsible**: validate carts of own group, choose budget codes for orders, have an overview of the spending.

## How is a newcomer assigned to a group?

After first log in to Quarks, the Support team will attribute a role and a group to the newcomer.

# Help needed?

In case of any question, request or issue, please send a [ticket](https://service.uni.lu/sp?id=sc_category&sys_id=ad5c24fedba3fc10e622d3ca4b961971&catalog_id=09863281db683c905c72ef3c0b9619dc&spa=1).
