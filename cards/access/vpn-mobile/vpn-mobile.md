---
mount: access/vpn-mobile
name: "VPN connection on your mobile phone"
tags:
  - access
redirects:
  - access:vpn-mobile
  - /external/access/vpn-mobile/
  - /cards/access:vpn-mobile
  - /external/cards/access:vpn-mobile
  - /external/external/access/vpn-mobile/
---


# VPN connection on your mobile phone

## iPhone
1. In the App store, download "Cisco Secure Client".

![img1.png](img/img1.png){.w3-4}

2. Click on *Connections*.

![img2.png](img/img2.png){.w3-4}

3. Click on *Add VPN Connection...*.

![img3.png](img/img3.png){.w3-4}

4. In Server Address, write *vpn.uni.lu* and save.

![img4.png](img/img4.png){.w3-4}

5. Allow the *AnyConnect* to add VPN configurations.

![img5.png](img/img5.png){.w3-4}

6. Enter your UL credentials.

![img6.png](img/img6.png){.w3-4}

7. To turn on/off the VPN, slide the button *AnyConnect VPN*.

![img7.png](img/img7.png){.w3-4}
