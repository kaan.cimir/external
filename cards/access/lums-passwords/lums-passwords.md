---
mount: access/lums-passwords
name: "LUMS account"
tags:
  - access
redirects:
  - access:lums-passwords
  - /external/access/lums-passwords/
  - /cards/access:lums-passwords
  - /external/cards/access:lums-passwords
  - /external/external/access/lums-passwords/
---

# LUMS account

In addition to the standard university account, LCSB researchers and external collaborators are provided with the LUMS (LCSB User Management System) account.
This serves for internal authentication for resources provided by the LCSB, e.g. LCSB hosted [GitLab](https://gitlab.lcsb.uni.lu), LCSB [OwnCloud](https://owncloud.lcsb.uni.lu) storage or access to virtual machines.

## Requesting LUMS account as LCSB/DLSM members

  1. Go to [service.uni.lu](https://service.uni.lu).

  2. Login with your university credentials.

  3. Create a ticket requesting a LUMS account:

     *  Choose **LCSB** ticket service:

      ![lums_ticket-main.png](img/lums_ticket-main.png)

      *  Navigate to **"LCSB IT: Account Management Services"**, choose **" LUMS- New account"** and fill the [form](https://service.uni.lu/sp?id=sc_cat_item&sys_id=c536257ddb336010ca53454039961936&sysparm_category=6b76697ddb336010ca534540399619f8):

      ![lums_ticket-request.png](img/lums_ticket-request.png)

     * If you know already to which services you need access, please specify it in the ticket.

## Requesting LUMS account as external collaborator, UL members or  UL student

* In case you are not a member of LCSB/DLSM, please ask your collaborator at LCSB/DLSM to request the account for you. You will need to provide an institutional email address.
* Once the account is created, you will receive an email with your new LUMS account details and how to access services and VMS.

## Requesting LUMS account for externals collaborators, UL members or UL students

 When the account holder is an external collaborator,  UL member (but not a LCSB/DLSM member), or UL student :

  1. The account holder must have an account manager assigned. The account manager has to be a LCSB/DLSM member.

  2. The account manager should request the account on ServiceNow. A form with the following information will then be created, which the account manager must sign:

      * The full name of the account holder,
      * The institutional email,
      * and the validity period of the account, otherwise the  account will be valid until the 31st March of the following year.

  3. Access to Owncloud and GitLab needs to be approved by the account manager and access to other services or VMs need to be approved by the service or VM owner.

  4. At the end of the account validity, the account manager must sign a new form (as specified in step 2) in order to extend it. In case the assigned account manager changes, the new account manager must also sign a new form immediately and confirm the access permissions by informal approval. The assigned account manager must fulfill the criteria in 1), otherwise the account will be disabled immediately.

## (Re-)Activating LUMS account upon receiving credentials or after password reset

The password you will receive from the system administrators is temporary and **it will expire after one month**, so you have to reset it as soon as possible. It is usually sent as a link to [PrivateBin](/access/passwords). **This link is valid for one week and is deleted after you see it once**, thus you need to keep it open until you have followed these steps to reset your password:

  1. Go to [lums.uni.lu](https://lums.uni.lu) and login with your LUMS username and the temporary password you received. If you experience any issues at this step, try to use *Chrome* browser for this procedure. Should the login look different from this, e.g. like a pop-up, click on **x** or **Cancel** to close it.

  ![lums_login.png](img/lums_login.png)

  2. You will be prompted to reset your password:

  ![lums_first-reset-password.png](img/lums_first-reset-password.png)

  3. Enter your temporary password into the field **"Current Password"**.
  4. Leave the **"OTP"** field blank.
  5. Enter a new password of your choice in the field **"New Password"** and repeat the same password in the field **"Verify Password"**. This is the actual password you need to use from now on, so make sure to remember it.
  6. Click on **"Reset Password and Login"**.

It is a good practice to use a password manager which will not only help you to keep track of your passwords so you will never forget it, but it will also help you to generate passwords which are very safe (see [How-to card](/access/passwords) on password management for more detail).

## Changing password for LUMS accounts
If you want to change your password for the LUMS account, you can:
  1. Go to [lums.uni.lu](https://lums.uni.lu) and login with your LUMS credentials:

  ![lums_login.png](img/lums_login.png)

  2. In the top-right corner click on your name and select **Change password**:

  ![lums_home-settings.png](img/lums_home-settings.png)

  3. Enter your current password and your new password:

  ![lums_reset-password.png](img/lums_reset-password.png)
