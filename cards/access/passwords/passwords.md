---
mount: access/passwords
name: "Managing your passwords"
tags:
  - access
redirects:
  - access:passwords
  - /external/access/passwords/
  - /cards/access:passwords
  - /cards/access/passwords
  - /external/cards/access:passwords
  - /external/external/access/passwords/
---

# Managing your passwords

 There are several tools that can be used for password management. Some provide limited (basic) features for free, some require a fee. Below are some password management tools that can be used:

|Tool|Notes|
|-----|------------------------|
| [Bitwarden](https://bitwarden.com)| Free for most features and a cloud-based solution, which allows you to connect from multiple devices.|
| [dashlane](https://www.dashlane.com) |Free for limited features. |
| [KeePass](https://keepass.info)| Free but offline. |

# Tips when resetting University of Luxembourg Active Directory password on a Mac

To reset your UL AD password when using a Mac:

1. Go to <https://owa.uni.lu> and change your password there.
2. Before rebooting your Mac, you should change your keychain password to avoid any issues down the road. To do so, run the following command `security set-keychain-password "/Users/YOURHOME/Library/Keychains/login.keychain-db"`
3. If you use FileVault, after the first reboot, first enter your old password, then the new one. Normally, FileVault will sync and update with your new password after you reboot.

# Further information

- [Sharing passwords](/exchange-channels/sharingPasswords)
