---
mount: access/wifi-access-guests
name: "WiFi network access for guests"
tags:
  - access
redirects:
  - access/wifiNetworkAccessGuests
  - access:wifiNetworkAccessGuests
  - /external/access/wifiNetworkAccessGuests/
  - /cards/access:wifiNetworkAccessGuests
  - /external/cards/access:wifiNetworkAccessGuests
  - /external/external/access/wifiNetworkAccessGuests/
---

# WiFi network access for guests

- Each UL staff member can provide wireless network access for her/his visitors by creating an account on the [Guest Sponsor Portal](https://sponsor.uni.lux:8443/sponsorportal/PortalSetup.action?portal=06f49c40-ad3a-11e4-ba4b-0050568059d7).
- In order to create the guest account, you must log in with your normal UL username and password.
- Such a visitor account has a lifetime of maximum 5 days. If you need accounts for people staying longer, please open a [ticket via the SIU helpdesk](https://service.uni.lu/sp).
- Guests will only have access to the internet and public services but not to internal resources.

Other documentations on how to use the SIU systems can be found on the [UL intranet (section SIU-SIU-Guides-Wireless Network)](https://uniluxembourg.sharepoint.com/sites/siu/SitePages/siu-guides.aspx).

For any questions please open a [ticket via the SIU helpdesk](https://service.uni.lu/sp).

[comment]: Process owner: Julia Kessler
